﻿namespace CorumGroupPuzzles.Core.LinkedList
{
    using CorumGroupPuzzles.Core.LinkedList.Exceptions;

    public class LinkedListPresenter<T>
    {
        #region Private Fields

        /// <summary>
        /// LinkedList view.
        /// </summary>
        private ILinkedListView<T> view;

        /// <summary>
        /// The Generic linked list.
        /// </summary>
        private SinglyLinkedList<T> list;

        #endregion
 

        public LinkedListPresenter(ILinkedListView<T> view)
        {
            this.view = view;
            this.list = new SinglyLinkedList<T>();
        }

        #region Public Methods

        /// <summary>
        /// Add to List
        /// </summary>
        public void AddToList()
        {
            if (string.IsNullOrWhiteSpace(this.view.TextElements))
            {
                this.view.TextElements += string.Format("{0}", this.view.Element);
            }
            else
            {
                this.view.TextElements += string.Format(" - {0}", this.view.Element);
            }

            this.list.Insert(this.view.Element);
        }


        ///<summary>
        /// Gets the element by position
        /// </summary>
        /// <param name="position">The position.</param>
        /// <returns></returns>
        public T GetElement(int position)
        {
            if (string.IsNullOrEmpty(this.view.TextElements))
                    throw new EmptyListException();

            return list.GetItemFromTail(position);
        }

        /// <summary>
        /// Clears the list.
        /// </summary>
        public void Clear()
        {
            this.list.Clear();
            this.view.TextElements = string.Empty;
        }


        #endregion
    }
}