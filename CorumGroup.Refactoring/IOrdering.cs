﻿namespace CorumGroup.Refactoring
{
    //Refactoring: apply SOLID principles: Interface Segregation principle
    //Please scroll down. NOTE: keeping solution in the same file for clarity and readability
    interface IOrderingX
    {
        void GetCustomeriPad();
        void ProcessOrder();
        void GetNameGalaxyS5();
        void GetCount();
        void UpdateCustomer();
        void iPhoneProcessOrder();
        void GetCustomer();
        void GetName();
        void SaveOrder();
        void GetCountiPhone();
        void GalaxyS5GetCustomer();
        void ProcessOrderiPad();
        void SaveCustomer();
        void GetOrder();
    }

    #region refactored interfaces

    public enum DeviceType
    {
        iPad,
        iPhone,
        GalaxyS5
    }

    public interface ICustomer {
        void GetCustomer(DeviceType type);
        void UpdateCustomer();
        void SaveCustomer();
    }
    public interface IOrdering {
        void GetOrder(DeviceType type);
        void ProcessOrder(DeviceType type);
        void SaveOrder();
    }
    public interface IProduct {
        void GetName(DeviceType type);
        void GetCount(DeviceType type);
    }

    #endregion
}
