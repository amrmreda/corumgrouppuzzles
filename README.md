Corum Coding Questions (Write your solutions in C# .NET)

#### Problem 1 
Refactoring. The following interface is a simple ordering contract for an e-commerce system. For the purposes of this exercise the input & output types have been deliberately omitted to keep it simple. It was originally written to support a website, but over time has been enhanced to support various mobile clients like iPhone, iPad, Galaxy S5. This contract is not SOLID. Apply SOLID principles to refactor the contract.

interface IOrdering

{

void GetCustomeriPad();

void ProcessOrder();

void GetNameGalaxyS5();

void GetCount();

void UpdateCustomer();

void iPhoneProcessOrder();

void GetCustomer();

void GetName();

void SaveOrder();

void GetCountiPhone();

void GalaxyS5GetCustomer();

void ProcessOrderiPad();

void SaveCustomer();

void GetOrder();

}

#### Problem 2 
Triangle Classification. Write an application that classifies a triangle given the length of the 3 sides. E.g. the inputs (4, 4, 4) would be an equilateral triangle.

#### Problem 3
String Reversal. Write an application that reverses the words in sentence e.g.

string sentence = "This is a test";

string sentenceReversed = "sihT si a tset";

#### Problem 4 
Linked List Traversal. Write an application that returns the 5th last element of a list e.g.

LinkedListItem l = new LinkedListItem(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

public class LinkedListItem

{

// TODO: member variables

public LinkedListItem(params int[] args)

{ TODO }

public int GetAtEndOffset(int offset)

{ TODO }

}

Assert.AreEqual(6, l.GetAtEndOffset(5), "This was not the 5th last element"); 