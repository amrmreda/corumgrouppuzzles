﻿namespace CorumGroupPuzzles.UI
{
    using System;
    using System.Windows;
    using CorumGroupPuzzles.Core.ReverseWords;

    /// <summary>
    /// Interaction logic for ReverseWordsView.xaml
    /// </summary>
    public partial class ReverseWordsView : Window, IReverseWordsView
    {
        #region Private Fields

        private ReverseWordsPresenter presenter;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ReverseWordsView"/> class.
        /// </summary>
        public ReverseWordsView()
        {
            InitializeComponent();

            this.presenter = new ReverseWordsPresenter(this);
        }

        #region Implementation of IReverseWordsView

        /// <summary>
        /// Gets or sets the source text.
        /// </summary>
        /// <value>The source.</value>
        public string Source
        {
            get
            {
                return this.SourceTextBox.Text;
            }
            set
            {
                this.SourceTextBox.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the destination text.
        /// </summary>
        /// <value>The destination.</value>
        public string Destination
        {
            get
            {
                return this.DestinationTextBox.Text;
            }
            set
            {
                this.DestinationTextBox.Text = value;
            }
        }

        #endregion


        #region Event Handling

        /// <summary>
        /// Handles the Click event of the btnConvert control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ConvertButtonClick(object sender, EventArgs e)
        {
            this.presenter.Reverse();
        }

        #endregion
    }
}
