﻿namespace CorumGroupPuzzles.UI
{
    using System.Windows;

    using CorumGroupPuzzles.Core.LinkedList;

    /// <summary>
    /// Interaction logic for LinkedListView.xaml
    /// </summary>
    public partial class LinkedListView : Window, ILinkedListView<int>
    {
        /// <summary>
        /// Element index to be retrieved
        /// </summary>
        public const int FIFTH_ELEMENT_INDEX = 5;

        #region Private Fields

        private LinkedListPresenter<int> presenter;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkedListView"/> class.
        /// </summary>
        public LinkedListView()
        {
            InitializeComponent();

            this.presenter = new LinkedListPresenter<int>(this);
        }

        #region Implementation of ILinkedListView

        /// <summary>
        /// Gets or sets the array elements.
        /// </summary>
        /// <value>The array elements.</value>
        public string TextElements
        {
            get { return this.txtElement.Text; }
            set { this.txtElement.Text = value; }
        }

        /// <summary>
        /// Gets or sets the element to be added to the array.
        /// </summary>
        /// <value>The element to be added to the array.</value>
        public int Element
        {
            get { return int.Parse(this.txtElements.Text); }
            set { this.txtElements.Text = value.ToString(); }
        }

        #endregion
         
        #region Event Handling
        
        /// <summary>
        /// Handles the Click event of the AddButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void AddButtonClick(object sender, RoutedEventArgs e)
        {
            presenter.AddToList();
        }

        /// <summary>
        /// Handles the Click event of the ClearButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ClearButtonClick(object sender, RoutedEventArgs e)
        {
            presenter.Clear();
        }

        /// <summary>
        /// Handles the Click event of the Get5thElementButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void Get5thElementButtonClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(
            string.Format("{0} {1}", Properties.Resources.TheFifthElement, presenter.GetElement(FIFTH_ELEMENT_INDEX)),
            Properties.Resources.Results,
            MessageBoxButton.OK,
            MessageBoxImage.Information);
        } 
        #endregion
    }
}
